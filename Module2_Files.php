<!DOCTYPE html>
<?php
    session_start();
    if (!isset($_SESSION['username'])){
        header("Location: Module2_Login.php");
        exit;
    }
    
    $username = $_SESSION['username'];
    
    //Check for pictures to delete.
    if (isset($_GET['delete'])){
        $toDelete = $_GET['delete'];
        $deletePath = sprintf("/srv/module2_private/%s/%s", $username, $toDelete);
        if (file_exists($deletePath)){
            unlink($deletePath);
        }
    }
    
    
    //Check for uploaded files.
    // Validate the file name. Replace any spaces with underscores.
    if (isset($_FILES['uploadedfile'])){
        $filename = basename($_FILES['uploadedfile']['name']);
        $filename = str_replace(' ', '_', $filename);
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
                echo "Invalid filename";
        }else{
    
            $uploadPath = sprintf("/srv/module2_private/%s/%s", $username, $filename);
             
            if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $uploadPath) ){
                    echo "File successfully uploaded.";
            }else{
                    echo "Could not upload file.";
            }
        }
    }
    //Determine user's filepath and files.
    $userPath = sprintf("/srv/module2_private/%s", $username);
    $userFiles = scandir($userPath);
    //Remove extraneous folders.
    unset($userFiles[0]);
    unset($userFiles[1]);
    
    
    
    
?>

<script>
    function confirmDelete(){
        var del = window.confirm("Are you sure you want to delete this file?");
        if (del) {
            document.forms["files"].action = "Module2_Files.php";
            document.forms["files"].submit();
        }
    }
    
</script>

<html>
<head>
    <title>Your Files</title>
    <link rel="stylesheet" type="text/css" href="/~jphilps/Module2CSS.css" />
</head>

<body class = "filespage">
    <div id="header" class="header">
        <h2> Welcome, 
            <?php
                echo $_SESSION['username'];
            ?>
        </h2>
    </div>
    <div class="leftpane">
        <form enctype="multipart/form-data" action="Module2_Files.php" method="POST">
        <p>
                <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
                <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
        </p>
        <p>
                <input type="submit" value="Upload File" />
        </p>
        </form>
    </div>
    <div id="files" class="leftpane">
       
        <form id="files" action="Module2_Display.php" method="GET">
        <?php
            foreach($userFiles as $filename){
                //Display view, delete buttons and files.
                echo "<button type=\"submit\" name=\"filename\" value=$filename>View</button><button type=\"submit\" onclick=\"confirmDelete()\" name=\"delete\" value=$filename>Delete</button>&nbsp&nbsp&nbsp", "$filename", "<br>";
            }
        ?>
        </form>
        <audio controls>
        <source src="horse.ogg" type="audio/ogg">
        <source src="horse.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
        </audio>
    </div>
    <div id="view">
        
        
    </div>

</body>
</html>
<?php
session_start();
$filename = $_GET['filename'];
// Obtain the file name and user name from the session.
$username = $_SESSION['username'];
//Build the file path.
$filepath = sprintf("/srv/module2_private/%s/%s", $username, $filename);
// Get the mime type.
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($filepath);
// Display the file.

header("Content-Type: $mime");
readfile($filepath);

?>
<!DOCTYPE html>

<?php 
	$realusers = array();
	$testuser = "";
	$validuser = false;
	
	if(isset($_GET['username'])){
		$testuser = $_GET['username'];
	}
	$h = fopen("/srv/module2_private/users.txt", "r");
	$linenum = 1;
	while( !feof($h) ){
		$realusers[] = fgets($h);
		$linenum++;
	}
	fclose($h);
	for($i=0; $i<count($realusers); $i++){
		if($realusers[$i] != ""){
			if (strcmp(trim($testuser), trim($realusers[$i])) == 0){
				$validuser = true;
			}
		}
	}
	if($validuser == true){
		session_start();
		$_SESSION['username'] = $testuser;
		echo "matched";
		header("Location: Module2_Files.php");
		exit;
	}
?>
<html>
<head>
	<meta charset="utf-8">
	<title> File Share Login </title>
	<link rel="stylesheet" type="text/css" href="/srv/module2_private/Module2CSS.css" />
</head>
<body class = "page">
	<p class = "header"> FileGarage </p>
	<form method="GET">
		<label class = "decorated"> Username: <input type="text" name="username" /></label>
		<input type="submit" value="Login" />
	</form>
</body>
</html>
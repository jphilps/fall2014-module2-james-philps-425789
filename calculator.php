<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title> Calculator </title>
	<link rel="stylesheet" type="text/css" href="/~jphilps/Module2CalcCSS.css" />
</head>
<body class = "calculator">
	<div class = calcheader> Calculate </div>
	<form method="GET" class="calcform">
		<label> <input type="text" name="box1" /></label>
		<input type="radio" name="operation" value="+">+
		<input type="radio" name="operation" value="-">-
		<input type="radio" name="operation" value="*">*
		<input type="radio" name="operation" value="/">/<br>
		<label> <input type="text" name="box2" /></label>
		<input type="submit" value="Go" /><br>
		<?php
		$display = 0;
			if(isset($_GET['box1'])){
				if(isset($_GET['box2'])){
					$var1 = $_GET['box1'];
					$var2 = $_GET['box2'];
					if(is_numeric($var1) && is_numeric($var2)){
						if(isset($_GET['operation'])){
							if($_GET['operation'] == "+"){
								$display = $var1 + $var2;
							}
							if($_GET['operation'] == "-"){
								$display = $var1 - $var2;
							}
							if($_GET['operation'] == "*"){
								$display = $var1 * $var2;
							}
							if($_GET['operation'] == "/"){
								if($_GET['box2'] == 0){
									$display = "Cannot divide by 0";
								}
								else{
									$display = $var1 / $var2;
								}
							}
						}
						else{
							$display = "Error: Please fill in all required fields.";
						}
					}
					else{
						$display = "Please enter numerical values only.";
					}
				}
				else{
					$display = "Error: Please fill in all required fields.";
				}
			}
			else{
				$display = "Please fill in all required fields then press Go.";
			}
		?>
		<label> Result: <br><input class="result" type="text" name="result" value= "<?php echo (isset($display))?$display:''; ?>" /></label>
	</form>
</body>
</html>